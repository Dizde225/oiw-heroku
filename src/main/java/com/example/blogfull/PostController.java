package com.example.blogfull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
public class PostController {

    @Autowired
    PostApi postApi;

    @GetMapping("/post")
    public void getPost(){
        System.out.println(postApi.getAllPosts().toString());
    }


    @PostMapping("/post")
    public String addPost(HttpServletRequest httpServletRequest, Model model){
        String tittle = httpServletRequest.getParameter("tittle");
        String content = httpServletRequest.getParameter("content");
        postApi.addPost(new Post(tittle,content));
        return "Success";
    }
}
