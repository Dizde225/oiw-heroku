package com.example.blogfull;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.ArrayList;
import java.util.List;

@Controller
public class ListTest {

    @GetMapping("/b" +
            "" +
            "")
    public String name(Model model){
        List<String> list = new ArrayList<>();
        list.add("a");
        list.add("b");
        model.addAttribute("listobiektow",list);
        return "listTest";

    }
}
