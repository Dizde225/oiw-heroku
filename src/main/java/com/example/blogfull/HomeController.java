package com.example.blogfull;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
public class HomeController {

    @GetMapping("/a")
    public String hello(){
        return "helloform";
    }

    @PostMapping("/a")
    public String index(HttpServletRequest httpServletRequest, Model model){
        String name = httpServletRequest.getParameter("name");
        if(name== null)
        {
            name = "world";
        }

        model.addAttribute("name",name);
        return "index2";
    }
}
