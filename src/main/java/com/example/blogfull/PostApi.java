package com.example.blogfull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Component
public class PostApi {

    private List<Post> allPosts;

    @Autowired
    private PostRepository postRepository;

    public void addPost(Post post){
        postRepository.save(post);
    }

    public void removePost(int id){
        postRepository.deleteById(id);
    }

    public List<Post> getAllPosts(){
        List<Post> temp = new ArrayList<>();
        for (Post post : postRepository.findAll()
             ) {
            temp.add(post);
        }
        Collections.reverse(temp);
        return temp;
    }

    public void updatePost(int id, String title, String content){
        Post post = postRepository.findById(id);
        if (title != null){
            post.setTittle(title);
        }
        if(content != null){
            post.setContent(content);
        }
        postRepository.save(post);
    }
}
