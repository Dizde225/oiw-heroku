package com.example.blogfull;

import org.springframework.data.repository.CrudRepository;

public interface PostRepository extends CrudRepository<Post, Integer> {
    public Post findById(int id);
}
